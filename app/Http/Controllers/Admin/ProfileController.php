<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SaveChangePwdRequest;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $userInfo = Auth::user();
        return view('admin.profile.form',compact('userInfo'));
    }

    public function save(Request $rq)
    {
        $rq->validate([
            'fullname' => 'required|max:30',
            'image' => 'mimes:jpeg,bmp,png'
        ]);
        $model = User::find(Auth::user()->id);
        $model->update($rq->all());
        if($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid()."-".$file->getClientOriginalName();
            $file->storeAs('uploads',$fileName); //->resize(320, 240)
            $model->image = 'uploads/'.$fileName;
//            dd('done');
        }
        $model->save();
        return redirect(route('homepage'));
    }

    public function changePwd()
    {
        return view('admin.profile.changePwd');
    }
    public function saveChangePwd(SaveChangePwdRequest $rq)
    {
//        dd(Hash::check($rq->old_password,Auth::user()->password));
        if(Hash::check($rq->old_password,Auth::user()->password))
        {
            $newPass = Hash::make($rq->new_password);
            Auth::user()->password = $newPass;
            Auth::logout();
            return redirect(route('login'));
        }
        return redirect(route('profile.changePwd'))->with('errMsg','Invalid old password !');
    }


}
