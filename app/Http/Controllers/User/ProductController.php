<?php

namespace App\Http\Controllers\User;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;


class ProductController extends Controller
{
    public function product(Request $rq)
    {
        $keyword = $rq->keyword;
        if ($keyword !=null)
        {
            $products = Product::where('product_name','like',"%$keyword%")->get();
//            $products->withPath("?keyword=$keyword");
        }else{
            $products = Product::all()->take(9);
    }
//            dd($products);
//        $products = DB::table('products')->limit(12)->get();
        $cate = Category::all();

//        dd($products);
        return view('user.product.list-product',compact('products','cate','keyword'));
    }

    public function productDetail($slug)
    {
        $product = Product::where('slug',$slug)->first();
        $cate = Category::all();
        return view('user.product.product-detail',compact('product','cate'));
    }

    public function addCart(Request $rq)
    {
        dd($rq);
    }
}
