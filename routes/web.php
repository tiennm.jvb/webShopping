<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
 *                  Home Page
 */
Route::get('/','HomeController@index')->name('home');



/*
 *                              Profile
 */
Route::group(['prefix' => 'profile','middleware'=>'member'], function(){
    Route::get('/','User\ProfileController@update')->name('profile');
    Route::post('/save','User\ProfileController@save')->name('profile.save');
    Route::get('/change-pwd','User\ProfileController@changePwd')->name('changePwd');
    Route::post('/change-pwd','User\ProfileController@postChangePwd')->name('postChangePwd');
});




/*
 *                          Category Client
 */
Route::get('category','User\CategoryController@index')->name('home.category');
Route::get('category-detail/{slug}','User\CategoryController@detail')->name('home.categoryDetail');




/*
 *                          Product Client
 */
Route::get('/order','HomeController@order')->name('home.order');
Route::get('/products','User\ProductController@product')->name('home.product');
Route::get('/product-detail/{slug}','User\ProductController@productDetail')->name('home.productDetail');



/*
 *                             News Client
 */
Route::get('/news','User\NewsController@index')->name('home.news');
Route::get('/news-detail/{slug}','User\NewsController@detail')->name('home.news-detail');



/*
 *                              Cart
 */
Route::post('/url_cart','HomeController@urlCart')->name('addCart');
Route::get('/view-cart','HomeController@viewCart')->name('viewCart');
Route::get('/cart-Item','Controller@cartItem')->name('cartItem');


//Route::get('/cart-Item','HomeController@cartItem')->name('cartItem');


//Route::get('/add-cart','User\ProductController@addCart')->name('addCart');



// Begin Homepage


/*
 *                          Begin Admin Login
 */

Route::get('/admin-login', 'Auth\LoginController@login')->name('login');
Route::post('/admin-postLogin', 'Auth\LoginController@postLogin')->name('postLogin');
Route::any('/admin-logout',function (){
    Auth::logout();
    return redirect(route('login'));
})->name('logout');




/*
 *                          Begin User login
 */
Route::get('/user-login', 'UserLoginController@login')->name('user.login');
Route::post('/user-postLogin', 'UserLoginController@postLogin')->name('post-user.login');
Route::any('/user-logout',function (){
    Auth::logout();
    return redirect(route('user.login'));
})->name('user-logout');



/*
 *                          Begin User register
 */
Route::get('register','RegisterController@index')->name('register');
Route::post('post-register','RegisterController@create')->name('PostRegister');
Route::get('verify-register{token}','RegisterController@verify')->name('verify_token');

//
//Route::get('register', 'RegisterController@index')->name('register');
//Route::post('register', 'RegisterController@createUser');
//Route::get('verify/register/{token}', 'RegisterController@verify')->name('verify_user');
//




use Illuminate\Support\Facades\Mail;
//Route::get('send-mail',function (){
//    $username = "Nguyen Manh Tien";
//    Mail::send('mail_template.test-send-mail',compact('username'),function ($message){
//        $message->to('tiennm1197@gmail.com','Nguyen Tien');
//        $message->cc('tiennm249@gmail.com','hiiii');
//        $message->replyTo('tiennm.jvb@gmail.com','hihi');
//        $message->subject('test mail');
//    });
//    return 'done';
//});

Route::get('send-mail', function() {
    $username = 'tien';
    Mail::send('mail_template.test-send-mail', compact('username'), function ($message) {
        $message->to('tiennm1197@gmail.com', 'Tien dz');
        $message->subject('test email');
    });
    return 'done!';
});

use Illuminate\Http\Request;
use App\PasswordReset;
use Carbon\Carbon;
use App\User;
Route::post('forget-pwd-email', function(Request $request) {
    $email = $request->email;
    $user = User::where('email', $email)->first();
    if(!$user) {
        return 'done!';
    }
    $pwdReset = PasswordReset::where('email', $request->email)->delete();
    $token = str_random(20);
    $now = Carbon::now();
    $pwdReset = new PasswordReset();
    $pwdReset->email = $request->email;
    $pwdReset->token = $token;
    $pwdReset->created_at = $now;
    $pwdReset->save();

    $url = url('/reset-pwd/'.$token);
    // send email
    Mail::send('mail_template.reset-password-mail', compact('url', 'user'), function ($message) use ($user) {
        $message->to($user->email, $user->name);
        $message->subject('Yêu cầu cấp lại mật khẩu');
    });
    return 'done!';
})->name('forget-pwd.email');



Route::get('/reset-pwd/{token}',function ($token){
    // check token co hop le k
    $pwcReset = PasswordReset::where('token',$token)->first();
    if (!$pwcReset)
    {
        return "errors invalid token";
    }
    $thatDay = Carbon::createFromFormat('Y-m-d H:i:s', $pwcReset->created_at);
    $now  = Carbon::now();
    $dif = $now->diffInHours($thatDay);
    if ($dif > 24)
    {
        $pwcReset->delete();
        return "error! invalid token";
    }
    return view('auth.reset-pwd',compact('token'));
//    dd($dif);
//    return $token;
});




Route::post('auth.reset-pwd',function (Request $rq){
    // check token co hop le k
    $pwcReset = PasswordReset::where('token',$rq->token)->first();
    if (!$pwcReset)
    {
        return "errors invalid token";
    }
    $thatDay = Carbon::createFromFormat('Y-m-d H:i:s', $pwcReset->created_at);
    $now  = Carbon::now();
    $dif = $now->diffInHours($thatDay);
    if ($dif > 24)
    {
        $pwcReset->delete();
        return "error! invalid token";
    }
    $user = App\User::where('email',$pwcReset->email)->first();
    $user->password = Hash::make($rq->password);
    $user->save();
    return "Done";
})->name('auth.reset-pwd');



