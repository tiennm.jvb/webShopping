@extends('layouts.ClientTheme')
@section('title','Category')
@section('content')

<section class="banner bgwhite p-t-40 p-b-40">
    <div class="container">
        <div class="container">
            <div class="row">

                    @foreach($cate as $c)
                    @if($cate =! null)
                        @if($c->status ==1)
                            <div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
                                <!-- block1 -->
                                <div class="block1 hov-img-zoom pos-relative m-b-30">
                                    <img src="{{asset($c->image)}}" width="360" height="466" alt="IMG-BENNER">

                                    <div class="block1-wrapbtn w-size2">
                                        <!-- Button -->
                                        <a href="{{ route('home.categoryDetail',['slug'=>$c->slug]) }}" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
                                            {{ $c->category_name }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection