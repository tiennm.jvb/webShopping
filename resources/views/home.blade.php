
@extends('layouts.ClientTheme')
@section('title','Home Page')
@section('content')
<!-- Slide1 -->
<!-- Slide1 -->
<section class="slide1">
    <div class="wrap-slick1">
        <div class="slick1">
            @foreach($slider as $sl)
                    <div class="item-slick1 item1-slick1" style="background-image: url({{asset($sl->image)}});">
                        <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
                                <span class="caption1-slide1 m-text1 t-center animated visible-false m-b-15" data-appear="{{ $a }}">
                                    Women Collection 2018
                                </span>
                            <h2 class="caption2-slide1 xl-text1 t-center animated visible-false m-b-37" data-appear="{{ $b }}">
                                New arrivals
                            </h2>

                            <div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="{{ $c }}">
                                <!-- Button -->
                                <a href="product.html" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
                                    Shop Now
                                </a>
                            </div>
                        </div>
                    </div>
            @endforeach
        </div>
    </div>
</section>
<!-- Banner -->
<section class="banner bgwhite p-t-40 p-b-40">
    <div class="container">
    <div class="container">
        <div class="row">
            @foreach($cate as $c)
                <div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
                <!-- block1 -->
                <div class="block1 hov-img-zoom pos-relative m-b-30">
                    <img src="{{asset($c->image)}}" width="360" height="446" alt="IMG-BENNER">

                    <div class="block1-wrapbtn w-size2">
                        <!-- Button -->
                        <a href="{{ route('home.categoryDetail',['slug'=>$c->slug]) }}" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
                            {{ $c->category_name }}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    </div>
</section>
<!-- New Product -->
<section class="newproduct bgwhite p-t-45 p-b-105">
    <div class="container">
        <div class="sec-title p-b-60">
            <h3 class="m-text5 t-center">
                Featured Products
            </h3>
        </div>

        <!-- Slide2 -->
        <div class="wrap-slick2">
            <div class="slick2">
                @foreach($product as $pr)
                <div class="item-slick2 p-l-15 p-r-15">
                    <!-- Block2 -->

                    <div class="block2">
                        <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                            <img src="{{asset($pr->image)}}" width="270" height="324" alt="IMG-PRODUCT">

                            <div class="block2-overlay trans-0-4" id="pro-1">
                                <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                </a>

                                <div class="block2-btn-addcart w-size1 trans-0-4">
                                    <!-- Button -->
                                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4 add-cart" rel="{{ $pr->id }}"    >
                                        Add to Cart
                                    </button>
                                    <input type="hidden"  value="1" class="countNumber">
                                </div>
                            </div>
                        </div>

                        <div class="block2-txt p-t-20">
                            <a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5 " id="ProductName">
                                {{ $pr->name }}
                            </a>

                            <span class="block2-price m-text6 p-r-5">
									$ {{ $pr->price }}
								</span>
                        </div>
                    </div>
                </div>
                @endforeach
                    <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
            </div>
        </div>
    </div>
</section>
<section class="blog bgwhite p-t-94 p-b-65">
    <div class="container">
        <div class="sec-title p-b-52">
            <h3 class="m-text5 t-center">
                Our Blog
            </h3>
        </div>
            <div class="row">
                @foreach($product as $p)
            <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
                <!-- Block3 -->
                <div class="block3">
                    <a href="blog-detail.html" class="block3-img dis-block hov-img-zoom">
                        <img src="{{asset($p->image)}}" width="370" height="277" alt="IMG-BLOG">
                    </a>

                    <div class="block3-txt p-t-14">
                        <h4 class="p-b-7">
                            <a href="{{ route('home.productDetail',['slug'=>$p->slug]) }}" class="m-text11">
                                {{ $p->product_name }}
                            </a>
                        </h4>
                        <span class="s-text6">By</span> <span class="s-text7">
                        @foreach($user as $u)
                                @if($p->user_id == $u->id)
                                    {{ $u->name }}
                                @endif
                            </span>
                        @endforeach
                        <p><span class="s-text6">on</span> <span class="s-text7">{{ $p->created_at }}</span>
                        </p>
                        <p class="s-text8 p-t-16">
                            {!! str_limit($p->description,100,' ...') !!}
                        </p>
                    </div>
                </div>
            </div>
                @endforeach
        </div>

    </div>
</section>
@endsection
