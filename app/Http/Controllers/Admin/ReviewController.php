<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;

class ReviewController extends Controller
{
    public function index()
    {
        $review = Review::all();
        return view('admin.review.list',compact('review'));
    }
}
