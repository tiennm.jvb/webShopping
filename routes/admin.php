<?php
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Category;
use App\Product;
use App\News;
use App\User;

/*
 *          Home Page
 */
Route::get('/',function (){
    $cate = Category::count();
    $product = Product::count();
    $news = News::count();
    $user = User::count();
    return view('admin.dashboard',compact('cate','product','news','user'));
})->name('homepage');



/*
                Begin Category
        List - update - detail - delete
 */
Route::group(['prefix' => 'categories','middleware'=>'isAdmin'], function(){
    Route::get('/','CategoryController@index')->name('category.list');
    Route::get('/add','CategoryController@add')->name('category.add');
    Route::post('/save','CategoryController@save')->name('category.save');
    Route::get('/edit/{id}','CategoryController@edit')->name('category.edit');//->middleware('member')
    Route::get('/remove/{id}','CategoryController@remove')->name('category.remove');
    Route::get('/detail/{slug}','CategoryController@detail')->name('category.detail');
});



/*
                        Begin Product
List - update - detail - delete - Check name - check slug
 */
Route::group(['prefix' => 'products'], function(){
    Route::get('/','ProductController@index')->name('product.list');
    Route::get('/add','ProductController@add')->name('product.add');
    Route::get('/edit/{id}','ProductController@edit')->name('product.edit');
    Route::post('/save','ProductController@save')->name('product.save');
    Route::post('/check-product-name','ProductController@checkProductName')->name('products.checkName');
    Route::post('/check-product-slug','ProductController@checkProductSlug')->name('products.checkSlug');
    Route::post('/getSlug','ProductController@generateSlug')->name('getSlug');
    Route::get('/remove/{id}','ProductController@remove')->name('product.remove');
    Route::get('/detail/{slug}','ProductController@detail')->name('product.detail');
});


/*
                            Begin New
                List - update - detail - delete
 */
Route::group(['prefix' => 'news'], function(){
    Route::get('/','NewController@index')->name('news.list');
    Route::get('/add','NewController@add')->name('news.add');
    Route::post('/save','NewController@save')->name('news.save');
    Route::get('/edit/{id}','NewController@edit')->name('news.edit');
    Route::get('/remove/{id}','NewController@remove')->name('news.remove');
    Route::get('/detail/{slug}','NewController@detail')->name('news.detail');
});




/*
                        Begin User
        List - update - detail - delete - Check name
 */
Route::group(['prefix' => 'users','middleware'=>'isMasterAdmin'], function(){
    Route::get('/','UserController@index')->name('user.list');
    Route::get('/add','UserController@add')->name('user.add');
    Route::post('/save','UserController@save')->name('user.save');
    Route::post('check-name','UserController@CheckName')->name('user.CheckName');
    Route::get('/edit/{id}','UserController@edit')->name('user.edit');
    Route::get('/delete/{id}','UserController@delete')->name('user.delete');
    Route::get('/detail/{slug}','UserController@detail')->name('user.detail');
//    Route::get('/detail{slug}','ProductController@detail')->name('product.detail');
});



/*
                    Begin Review
 */
Route::group(['prefix'=>'review'],function (){
    route::get('/','ReviewController@index')->name('review.list');
});



/*
                    Begin Comment
 */
Route::group(['prefix'=>'comment'],function (){
    Route::get('/','CommentController@index')->name('comment.list');
});

/*
 *
 *                           Profile
 *                      profile and update
 */
Route::get('/admin-profile','ProfileController@update')->name('profile.update');
Route::post('/profile-save','ProfileController@save')->name('profile.save');
Route::get('/change-pwd','ProfileController@changePwd')->name('profile.changePwd');
Route::post('/change-pwd','ProfileController@saveChangePwd')->name('profile.saveChangePwd');



/*
*                          Slider
 */
Route::group(['prefix'=>'slider'],function (){
    Route::get('/','SliderController@index')->name('slider.list');
    Route::get('/add','SliderController@add')->name('slider.add');
    Route::get('/edit/{id}','SliderController@edit')->name('slider.edit');
    Route::get('/delete/{id}','SliderController@delete')->name('slider.delete');
    Route::post('/save','SliderController@save')->name('slider.save');
});



//
//    Route::group(['prefix'=> 'categories',function(){
//        Route::get('/',function (){
//            return "Danh sach danh muc";
//        })->name('cate.list');
//        Route::get('/add',function (){
//            return "add danh muc";
//        })->name('cate.add');
//        Route::get('/edit{id}',function ($id){
//            return "edit danh muc";
//        })->name('cate.edit');
//        Route::get('/remove{id}',function ($id){
//            return "remove danh muc";
//        })->name('cate.remove');
//    }]);
//    Route::group(['prefix'=> 'comments',function(){
//        Route::get('/',function (){
//            return "Danh sach comment";
//        })->name('comments.list');
//        Route::get('/reply{id}',function ($id){
//            return "tra loi binh luan";
//        })->name('comments.edit');
//        Route::get('/remove{id}',function ($id){
//            return "remove binh luan";
//        })->name('comments.remove');
//    }]);
?>