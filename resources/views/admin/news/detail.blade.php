@extends('layouts.AdminTheme')
@section('title','News - Detail')
@section('content')
    <div class="col-md-12 offset-0">
        <div class="pricing hover-effect">
            <div class="pricing-head">
                <h3>
                        {{ $products->title }}
                </h3>
                <h4>
                    <img src="{{asset($products->image)}}"
                         onerror="this.src='{{asset('img-1.jpg')}}'"
                         width="450" ><br>
                </h4>
            </div>
            <ul class="pricing-content list-unstyled">
                <li>
                    <i class="fa fa-star text-center"></i>
                    Status :
                    {{ $products->status==1? "active" : "Hide" }}
                </li>
                <li>
                    <i class="fa fa-shopping-cart text-center"></i>
                    Created by :
                </li>
            </ul>
            <div class="pricing-footer">
                <p>
                    {{ $products->description }}
                </p>
                {{--<a href="{{ route('product.edit') }}" class="btn yellow-crusta">--}}
                {{--Edit <i class="m-icon-swapright m-icon-white"></i>--}}
                </a>
            </div>
        </div>
    </div>
@endsection