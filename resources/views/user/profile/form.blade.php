@extends('layouts.ClientTheme')
@section('title','Profile')
@section('content')
    <br><br>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-md-offset-3">
            <h2>
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">  Profile</span>
                    </div>
                </div>
            </h2>
            <form id="cate-form" action="{{route('profile.save')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Username :</label>
                    <input id="fullname"  type="text" name="fullname" value="{{ old('name',$userInfo->name) }}">
                    @if($errors)
                        <span class="text-danger">{{$errors->first('name')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>full_name :</label>
                    <input id="full_name" class="form-control" type="text" name="full_name" value="{{ old('full_name',$userInfo->full_name) }}" placeholder="Điền họ tên">
                    @if($errors)
                        <span class="text-danger">{{$errors->first('full_name')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Ảnh đại diện: </label>
                    <img src="{{ asset($userInfo->image) }}" onerror="this.src='{{ asset('default-user.png') }}'" width="150" alt="">
                    <input class="form-control" type="file" name="image" id="image">
                </div>
                <div class="form-group">
                    <label>Email :</label>
                    <input id="email" class="form-control" type="text" name="email" value="{{ old('email',$userInfo->email) }}" placeholder="Điền Email">
                    @if($errors)
                        <span class="text-danger">{{$errors->first('email')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>phone :</label>
                    <input id="phone" class="form-control" type="text" name="phone" value="{{ old('phone',$userInfo->phone) }}" placeholder="Điền số điện thoại">
                    @if($errors)
                        <span class="text-danger">{{$errors->first('phone')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>address :</label>
                    <input id="address" class="form-control" type="text" name="address" value="{{ old('address',$userInfo->address) }}" placeholder="Điền địa chỉ">
                    @if($errors)
                        <span class="text-danger">{{$errors->first('address')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <input id="status" class="form-control" type="text" name="status" value="{{ old('status',$userInfo->status) }}">
                    @if($userInfo->status == 1 ? "Active" : "Hide") @endif
                </div>
                <div class="text-center">
                    <button class="btn btn-primary">Lưu</button>
                    <a href="{{ route('category.list') }}" class="btn btn-danger">Huỷ</a>
                </div>
            </form>
            <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
        </div>
        <div class="col-md-4"></div>
    </div>
@endsection