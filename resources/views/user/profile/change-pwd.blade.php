@extends('layouts.ClientTheme')
@section('title','Change Password')
@section('content')
    <br><br>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 col-md-offset-3">
            <h2>
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">  Change Password</span>
                    </div>
                </div>
            </h2>
            <br><br>
            <form id="cate-form" action="{{route('postChangePwd')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="text-center">
                    <div class="form-group">
                        <div class="bo12 of-hidden size19 m-b-20">
                            <input class="sizefull s-text7 p-l-18 p-r-18" type="password" name="old_password" placeholder="old_password *">
                        </div>
                        @if($errors)
                            <span class="text-danger">{{$errors->first('old_password')}}</span>
                        @endif
                        @if(session('msg'))
                            <span class="text-danger">{{ session('msg') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="bo12 of-hidden size19 m-b-20">
                            <input class="sizefull s-text7 p-l-18 p-r-18" type="password" name="new_password" placeholder="new_password *">
                        </div>
                        @if($errors)
                            <span class="text-danger">{{$errors->first('new_password')}}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="bo12 of-hidden size19 m-b-20">
                            <input class="sizefull s-text7 p-l-18 p-r-18" type="password" name="cfpassword" placeholder="cfpassword *">
                        </div>
                        @if($errors)
                            <span class="text-danger">{{$errors->first('cfpassword')}}</span>
                        @endif
                    </div>
                    <button class="btn btn-primary">Lưu</button>
                    <a href="{{ route('home') }}" class="btn btn-danger">Huỷ</a>
                </div>
            </form>
            <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
        </div>
        <div class="col-md-4"></div>
    </div>

@endsection