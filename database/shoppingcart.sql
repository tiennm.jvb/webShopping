/*
 Navicat Premium Data Transfer

 Source Server         : WEB
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : shoppingcart

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 20/06/2018 17:18:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carts
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(10) NOT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT 2,
  `parent_id` int(11) NULL DEFAULT -1,
  `is_menu` int(10) NULL DEFAULT NULL,
  `created_by` int(10) NULL DEFAULT -1,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (3, 'Danh muc 3', 'danh-muc-3', 1, 2, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (4, 'Danh muc 3', 'danh-muc-3', 1, 2, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (5, 'Danh muc 4', 'danh-muc-4', 2, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (6, 'Danh muc 4', 'danh-muc-4', 1, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (7, 'Danh muc 4', 'danh-muc-4', 1, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (8, 'Danh muc 4', 'danh-muc-4', 1, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (9, 'Danh muc 5', 'danh-muc-5', 2, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (10, 'Danh muc 5', 'danh-muc-5', 2, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (11, 'aad ca', 'asasdas', -1, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (12, 'aad ca', 'asasdas', -1, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (13, 'asdasd', 'adasd', 2, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (14, 'asdacaczxczxcasasd', 'qwascscasc', 2, 1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (15, 'ccaa', 'cc-aa', 2, -1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (16, 'ca1', 'ca1', 2, -1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (17, 'dc', 'dc', 2, -1, NULL, -1, NULL, NULL);
INSERT INTO `categories` VALUES (18, 'haskdh', 'jahsjdhjkh', 2, -1, NULL, -1, 'uploads/5b213bdc00fdd-2018-06-07.png', NULL);
INSERT INTO `categories` VALUES (19, 'cascasc', 'asdqw', 2, -1, NULL, -1, 'uploads/5b213bf37c04c-2018-06-08.png', NULL);

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL,
  `new_id` int(10) NOT NULL,
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(10) NOT NULL,
  `created_by` int(10) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (2, 'bai moi', '<p>bai moi nhaaaa</p>', 'uploads/5b22c5a2bb3fb-2018-06-14 (2).png', 1, NULL, NULL, NULL, NULL, 'bai-moi');

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` int(10) NOT NULL,
  `product_price` int(10) NULL DEFAULT NULL,
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cate_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) NOT NULL,
  `quantity` int(10) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('tiennm27@gmail.com', 'CZeEGaDPQmYIMqP2VRUK', '2018-06-17 13:57:34');
INSERT INTO `password_resets` VALUES ('tiennm1197@gmail.com', 'E3LsaxrMlFIPgz3tKOY3', '2018-06-17 14:19:18');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `product_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `price` decimal(10, 0) NULL DEFAULT NULL,
  `quantity` int(10) NULL DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT 2,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cate_id` int(10) NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `discount_price` decimal(10, 2) NULL DEFAULT NULL,
  `user_id` int(10) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (6, 'csaooi', '89', '<p>asdacsjklsjvklsjdvsdvsvksdk jskdjv;sjdcjsdc</p>', 89, 89, 1, 'dsa-ád', 2, 'uploads/5b260c870e2e9-2018-06-14 (7).png', 200.00, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (8, 'oiausdoiauscjascb', 'h8', '<p>akjsdkascjasklcabcjhafckcqwckjqwkjcqwdgqjhwckuqwliql</p>', 8, 78, 1, 'jahs-cd', 1, 'uploads/5b226e557cd7d-2018-06-14 (3).png', 200.00, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (9, 'đâsdasd', 'sp4', NULL, 2000000, 20, 2, 'tien-tien', 3, NULL, 200.00, NULL, NULL, NULL, NULL);
INSERT INTO `products` VALUES (21, 'sadasdsad', NULL, '<p>ssdsad</p>', 123456, 23, 1, 'sd-ds', 4, NULL, NULL, 9, NULL, NULL, NULL);
INSERT INTO `products` VALUES (22, 'bat caca', NULL, NULL, 23, NULL, 2, '[object Object]', 3, NULL, NULL, 9, NULL, NULL, NULL);
INSERT INTO `products` VALUES (23, 'dsdd', NULL, NULL, 23, NULL, 2, '[object Object]', 3, NULL, NULL, 9, NULL, NULL, NULL);
INSERT INTO `products` VALUES (24, 'cc cc', NULL, NULL, 2, NULL, 2, 'cc-cc', 3, NULL, NULL, 9, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `rating` int(10) NULL DEFAULT NULL,
  `comments` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT 0,
  `remember_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` int(255) NULL DEFAULT 100,
  `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `verify_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `phone` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (2, 'admin', 'tiennm27@gmail.com', NULL, '$2y$10$l3zP1NVb3xAtO7gc5HrAa.qvyvlmj3lE5ze7hZjiuxqGSMnZ9q.py', 1, 'O7M5o7Jl45mq7yBnM8JRNc9QkYAHbIyPEz0l7jxnJbpX54eUMEhnCACR2eep', 600, NULL, NULL, NULL, NULL, '2018-06-15 09:52:09', '2018-06-18 08:15:08', NULL);
INSERT INTO `users` VALUES (3, 'root', 'tiencoi249@gmail.com', NULL, '$2y$10$NTY4X8EIXrbcsq/A6v6oReoAYEm9wSfwDb0yvrikMAUjgPhlYQB56', 1, 'lJYbBZ2NwgMQheqZBnUjQg6GF6vwk0dzm27aBRT3IgTyGkNrBkSPG769PT19', 100, NULL, NULL, NULL, NULL, '2018-06-15 09:52:09', '2018-06-15 09:52:09', NULL);
INSERT INTO `users` VALUES (6, 'Melissa Hegmann', 'tiennm@gmail.com', NULL, '$2y$10$hnH61Ls68DXQyFvbDpOjj.4fzvxtLAUb304qyIDqjhdFa0Pkm6Wa2', NULL, NULL, 300, NULL, NULL, NULL, NULL, '2018-06-15 09:55:04', '2018-06-15 09:55:04', NULL);
INSERT INTO `users` VALUES (7, 'tienn', 'tien@gmail.com', 'uploads/5b276269d67e9-muồng hoàng yến.jpg', '$2y$10$XWm9/hGFAwdyyahbX7N/Q.vdw72fyHWeNkiC9Lj5VpsWwbCQouXT.', 1, '5xkNCdpd0xqxFYkKBiTGYGxDhZBppcyLjGlHy5dRcHU5zqC58jr10E0TXzYN', 600, NULL, NULL, NULL, NULL, '2018-06-18 07:42:33', '2018-06-18 07:42:33', NULL);
INSERT INTO `users` VALUES (8, 'zsasdasd', 'asd@asd.com', 'uploads/5b276423ed300-img.jpg', '$2y$10$pklmiFiJMkl1iNvCR6M/F.4WBBGDp2/v5ryrgXQx58nTcX9XgC9mS', 1, NULL, 900, NULL, NULL, NULL, NULL, '2018-06-18 07:49:56', '2018-06-18 07:49:56', NULL);
INSERT INTO `users` VALUES (9, 'dev', 'tiennm1197@gmail.com', NULL, '$2y$10$NTY4X8EIXrbcsq/A6v6oReoAYEm9wSfwDb0yvrikMAUjgPhlYQB56', 1, 'jmdK7AfW36zbQgDSE4EUwuVDCelHMLdNpdZkdGwt31NSzgxI9zM0k41aQscz', 900, NULL, NULL, NULL, NULL, '2018-06-19 15:39:34', NULL, NULL);
INSERT INTO `users` VALUES (12, 'ui', 'sd@sd.com', NULL, '$2y$10$VNEv2yTUBMZIrcrNxX8NUOs1oRXKl/dH1KontX.S06OJ4nB0L0Ts2', 0, NULL, 100, NULL, 'dXwMtm7jWGDvR4Vuy53hAF0DdOFSK6', '898', NULL, '2018-06-19 10:19:48', '2018-06-19 10:19:48', 98989);
INSERT INTO `users` VALUES (17, 'ddddđ1233123123123', NULL, NULL, '$2y$10$lm6VmNpTtE0QVqVlFwBf4OjtQUeThVswIH0T4PV20Z7YQEp/p9LQ.', 0, NULL, 100, NULL, '03RcqhtuPsSHD1LWo50yugLrEEGt7Z', NULL, NULL, '2018-06-20 07:22:13', '2018-06-20 07:22:13', NULL);
INSERT INTO `users` VALUES (18, 'sdsd', NULL, NULL, '$2y$10$S2RaeqharkA83j8aTm.58Ok.1mRtWB6txWZBixQUa07LuXd4RtAEC', 0, NULL, 100, NULL, '6mrqOjTBcTinbleRGBw4aP4ih0BSfI', NULL, NULL, '2018-06-20 07:37:09', '2018-06-20 07:37:09', NULL);
INSERT INTO `users` VALUES (19, 'sd', NULL, NULL, '$2y$10$Sf6fvINHSIiahbWRRIH7W.Vzyze5yvi9lLKx9UPLmiOcz.nInqA4m', 0, NULL, 100, NULL, 'PAtlFtiOPaQfH0uo6buNLAdLn0jgAB', NULL, NULL, '2018-06-20 07:51:58', '2018-06-20 07:51:58', NULL);
INSERT INTO `users` VALUES (20, '2d', NULL, NULL, '$2y$10$qYaJiv0ccoMqMQL.FxoSP.tVCEuCq973DWzuE0Bjcuixz17g6nPci', 0, NULL, 100, NULL, '3lAGaVLO9ln3U2rLAu1oENCCtMMliV', NULL, NULL, '2018-06-20 07:52:05', '2018-06-20 07:52:05', NULL);
INSERT INTO `users` VALUES (21, 'dca', NULL, NULL, '$2y$10$xT9XUBWtif5HxInVdr4bOeUIwvL77xjaWm96q0Pj7Oq10pXF2HNdq', 0, NULL, 100, NULL, 'dIIl8l3TNlw1FzWdmz7ZFofWPhaDAc', NULL, NULL, '2018-06-20 08:21:32', '2018-06-20 08:21:32', NULL);
INSERT INTO `users` VALUES (22, 'c2', NULL, NULL, '$2y$10$r.k084suo1CdTaXt7ZRwv.Az5llBdOVSNksowLTYOulEK3.iTyUiS', 0, NULL, 100, NULL, 'KNaTePZELDYyKoHietRpPvesNfYXMW', NULL, NULL, '2018-06-20 08:22:14', '2018-06-20 08:22:14', NULL);
INSERT INTO `users` VALUES (23, 'c', NULL, NULL, '$2y$10$e3kNlnsaOv9i1srwIkZQSeH2m0WSGkvphRJkHeYpqfUux3Y0jeqaq', 0, NULL, 100, NULL, 'cjCKyEK3xIR2f319VXUcyK39tOiq2l', NULL, NULL, '2018-06-20 08:22:22', '2018-06-20 08:22:22', NULL);
INSERT INTO `users` VALUES (24, 'dc', NULL, NULL, '$2y$10$ce/3iR9GNLgv7J44O1JbqecW2Y.muQcYPlhtLbCOlPjC6OXcLSj7C', 0, NULL, 100, NULL, 'RJGEiDqnVBTSEUg5qxwaBEPxGzg9c6', NULL, NULL, '2018-06-20 08:28:13', '2018-06-20 08:28:13', NULL);
INSERT INTO `users` VALUES (25, 'zxczczxczxczxczxczxczxczxczczxzxczxczxczxc', NULL, NULL, '$2y$10$DcHzmW0VEvyIEBTutPrcKOfjwdBxHPAea03Ae6vAsuJgdR7Al5CDm', 0, NULL, 100, 'ca', 'GV9XmnqALnwq5dfWiOsA9ytNUlBoIe', NULL, NULL, '2018-06-20 08:32:14', '2018-06-20 08:32:14', NULL);
INSERT INTO `users` VALUES (31, 'tiennm1997', 'tiennm.jvb@gmail.com', NULL, '$2y$10$/RD3l2tD31tOucC0E1V3jOv8s4XOAKsgDB3gXHZqjznQsOHQjoEHq', 0, NULL, 100, 'nguyen manh tien', 'OZRuHLTGTT9Gj1TYFwK0Tkx3URcu8e', 'ha noi', NULL, '2018-06-20 10:06:17', '2018-06-20 10:06:17', 984506398);

SET FOREIGN_KEY_CHECKS = 1;
