<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreOrder extends Model
{
    protected $table = 'pre_order';
    public $fillable = ['name','price','quantity','product_id','user_id','created_at'];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
