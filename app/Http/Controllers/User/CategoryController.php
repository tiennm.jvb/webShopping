<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $cate = Category::all()->take(6);
        return view('user.category.list',compact('cate'));
    }

    public function detail($slug)
    {
        $cate = Category::where('slug',$slug)->first();
        $product = Product::where('cate_id',$cate->id)->first();
        return view('user.category.detail',compact('product','cate'));
    }
}
