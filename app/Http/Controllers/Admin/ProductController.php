<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\User;
use Auth;
use Illuminate\Validation\Rule;
//use Image;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function index(Request $rq)
    {
        $keyword = $rq->keyword;
        if ($keyword != null)
        {
            $products = Product::where('product_name','like',"%$keyword%")->paginate(5);
            $products = withPath("?keyword=$keyword");
        }else{
            $products = Product::all();
        }
        $cate = Category::all();
        $user = User::all();
//        dd($cate);
        return view('admin.product.list',compact('products','cate','keyword','user'));
    }

    public function add()
    {
        $model = new Product();
        $cates = Category::all();
        return view('admin.product.form',compact('model','cates'));
    }

    public function edit($id)
    {
        $model = Product::find($id);
        $cates = Category::all();
        return view('admin.product.form',compact('model','cates'));
    }

    public function save(Request $rq)
    {
        if ($rq->id != null)
        {
            $model= Product::find($rq->id);
        }else{
            $model= new Product();
        }
        $model->fill($rq->all());

        // upload file
        if($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid()."-".$file->getClientOriginalName();//->resize(500, 500)
            Image::make($file)->resize(200, 200);
            $file->storeAs('uploads',$fileName);
            $model->image = 'uploads/'.$fileName;
//            dd('done');
        }
        // lấy ra id người đang đăng nhập rồi lưu lại
        $user_id = Auth::user()->id;
        $model->user_id = $user_id;
//        dd($model);
        $model->save();
        return redirect(route('product.list'));
    }

    public function remove($id)
    {
        $product = Product::find($id);
        if ($product != null)
        {
            $product->delete();
        }
        return redirect(route('product.list'));
    }

    public function detail($slug = null)
    {
        $products = Product::where('slug',$slug)->first();
        $cate = Category::all();
//        dd($products);
        return view('admin.product.detail',compact('products','cate'));
    }


    // Kiểm tra Product name đã tồn tại hay chưa
    public function checkProductName (Request $rq){
        $pro = Product::where('product_name',$rq->product_name)->first();
        $result = $pro == false ? true :false;
        return response()->json($result);
    }

    // Kiểm tra Đường dẫn đã tồn tại hay chưa
    public function checkProductSlug(Request $rq)
    {
        $pro = Product::where('slug',$rq->slug)->first();
        $result = $pro == false ? true :false;
        return response()->json($result);
    }


    // Sinh ra 1 đoạn slug dựa vào product name nhập trên input product_name
    public function generateSlug (Request $rq){
//        $now = Carbon::now();
        $rand = str_random(30);
        $result = str_slug($rq->value)."-".$rand;
        return response()->json(['data'=>$result]);
    }



}
