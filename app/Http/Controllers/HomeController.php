<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Slider;
use App\Category;
use App\Product;
use App\PreOrder;
use App\User;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $slider = Slider::all();
        $cate = Category::all()->take(6);
        $product = Product::all()->take(6);
        $user = User::all();
        $preOrder = PreOrder::all();
//        dd($cate);
        $i = 0;
        do{
            if ($i % 3 == 0)
            {
                $a = 'fadeInDown';
                $b = 'fadeInUp';
                $c = 'zoomIn';
            }elseif ($i % 3 == 1)
            {
                $a = 'rollIn';
                $b = 'lightSpeedIn';
                $c = 'slideInUp';
            }else{
                $a = 'rotateInDownLeft';
                $b = 'rotateInUpRight';
                $c = 'rotateIn';
            }
            $i++;
        }while($i < count($slider));
//        dd(count($slider));
        return view('home',compact('slider','a','b','c','cate','product','user','preOrder'));
    }

    public function cartItem()
    {
        dd(1);
//        $addCart = 0;
        $addCart = "tien";
        return view('layouts.ClientTheme', compact('addCart'));
//        return view('layouts.ClientTheme',compact('addCart'));
    }



    public function viewCart()
    {
        $preOrder = PreOrder::all();
        return view('user.cart',compact('preOrder'));
    }

    public function urlCart(Request $rq)
    {
        $product= Product::find($rq->pro_id);
        if (!empty($product))
        {
            $cart = PreOrder::where('product_id',$rq->pro_id)->where('user_id',Auth::user()->id)->first();
        }
        if (!empty($cart))
        {
//            dd($cart->quantity);
            $cart = DB::table('pre_order')->where('product_id',$rq->pro_id)->update(['quantity' => $cart->quantity + $rq->quantity]);
        }else{
            $cart = new PreOrder();
            $cart->product_id = $rq->pro_id;
            $cart->quantity = $rq->quantity;
            $cart->user_id = Auth::user()->id;
            $cart->image = $product->image;
            $cart->price = $product->price;
            $cart->name = $product->product_name;
            $cart->save();

        }
        $preOrder = DB::table('pre_order')->where('user_id',Auth::user()->id)->count();
//        dd($preOrder);
        return $preOrder;
//        dd($cart);
    }
}
