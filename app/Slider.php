<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';
    public $fillable = ['title','slug','status','image','created_at','updated_at','delete_at'];
}
