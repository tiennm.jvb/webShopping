<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SaveChangePwdRequest;

class ProfileController extends Controller
{
    public function update()
    {
        $userInfo = Auth::user();
        return view('user.profile.form',compact('userInfo'));
    }

    public function save(Request $rq)
    {
        $model = User::find(Auth::user()->id);
        $model->update($rq->all());
        if ($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid()."-".$file->getClientOriginalName();
            $file->storeAs('uploads');
            $model->image= 'upload/'.$fileName;
        }
        $model->save();
        return redirect(route('homePage'));
    }

    public function changePwd()
    {
        return view('user.profile.change-pwd');
    }

    public function postChangePwd(SaveChangePwdRequest $rq)
    {
        if (Hash::check($rq->old_password,Auth::user()->password))
        {
            $newPass = Hash::make($rq->new_password);
            Auth::user()->password = $newPass;
            Auth::logout();
            return redirect(route('user.login'));
        }
        return back()->with('msg','Invalid Old PassWord');
    }

}
