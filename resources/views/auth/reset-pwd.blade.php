<form action="{{ route('auth.reset-pwd') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">
    <div>
        New Password
        <input type="password" name="password" placeholder="Password">
    </div>
    <div>
        Confirm Password
        <input type="password" name="cfpassword" placeholder="Confirm Password">
    </div>
    <button type="submit">submit</button>
</form>