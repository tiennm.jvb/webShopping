<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;

class SliderController extends Controller
{
    public function index()
    {
        $slider = Slider::all();
//        dd($slider);
        return view('admin.slider.list',compact('slider'));
    }

    public function add()
    {
        $model = new Slider();
        return view('admin.slider.form',compact('model'));
    }

    public function edit($id)
    {
        $model = Slider::find($id)->first();
        return view('admin.slider.form',compact('model'));
    }

    public function delete($id)
    {
        $slider = Slider::find($id)->first();
            if ($slider != null)
            {
                $slider->delete();
                return redirect(route('slider.list'));
            }
    }

    public function save(Request $rq)
    {
        //         CUSTOM MESSAGES
        $messages = [
            'title.required' => 'Vui lòng nhập tên tiêu đề',
            'title.max' => 'Tiêu đề không quá 90 kí tự',
            'slug.required' => 'Đường dẫn không được để trống',
            'slug.unique' => 'Đường dẫn đã tồn tại , vui lòng nhập đường dẫn mới',
            'image.mimes' => 'Không đúng định dạng ảnh , vui lòng chọn đúng định dạng'
        ];

        $rq->validate([
            'title' => 'required|max:90',
            'slug' => 'required|unique:slider',
            'image' => 'mimes:jpeg,bmp,png'
        ],$messages);

        if ($rq->id != null)
        {
            $model = Slider::find($rq->id)->first();
        }else{
            $model = new Slider();
        }
        $model->fill($rq->all());

        // Upload Image
        if ($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid(). "-".$file->getClientOriginalName();
            $file->storeAs('uploads',$fileName);
            $model->image='uploads/'.$fileName;
        }
        $model->save();
        return redirect(route('slider.list'));
    }
}
