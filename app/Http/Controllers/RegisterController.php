<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation;

class RegisterController extends Controller
{
    public function create(Request $rq)
    {
        $messages = [
            'name.required' => 'Mời bạn nhập tên !',
            'full_name.max'=>'họ tên không quá 20 kí tự',
            'email.required'=>'Mời bạn nhập email',
            'email.email'=>'Mời bạn nhập đúng định dạng email',
            'phone.numeric'=>'Số điện thoại phải là số',
            'phone.min'=>'Số điện thoại phải lớn hơn 8 số',
        ];
        $rq->validate([
            'name' => 'required|string|alpha_dash|max:255|unique:users|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'full_name'=>'max:20',
            'email'=>'required|email',
            'phone'=>'numeric',
            'password' => 'required|min:6|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})',
            'cfpassword'=>'required|same:password|min:6',
        ],$messages);

        $model = new User();
        $model->fill($rq->all());
        $model->password = Hash::make($rq->password);
        $model->status = 0;
        $model->verify_token = str_random(30);
//        dd($model);
        if ($model->save()) {
            Mail::send('mail_template.verify_user', compact('model'), function ($message) use ($model) {
                $message->from('tiennm1197@gmail.com', 'Quản trị viên');
                $message->to($model->email, $model->name);
                $message->subject('Xác nhận đăng ký tài khoản');
            });
        } else {
            return redirect(route('user.login'));
        }
    }

    public function verify($token)
    {
        $user = User::where('verify_token',$token)->first();
        $user->save();
        return " Chúc mừng bạn đã đăng ký thành công . Mời bạn đăng nhập lại !";
    }
}
