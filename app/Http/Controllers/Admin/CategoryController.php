<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

class CategoryController extends Controller
{
    public function index(Request $rs)
    {
        $keword = $rs->keyword;
        if ($keword != null)
        {
            $category = Category::where('category_name','like',"%$keword%")->get();
        }else{
            $category = Category::all();
        }
//        $category = Category::all();
        return view('admin.category.list',compact('keword','category'));
    }

    public function add()
    {
        $model = new Category();
        $cates = Category::all();
//        dd($cates);
        return view('admin.category.form',compact('model','cates'));
    }

    public function edit($id)
    {
        $model = Category::find($id);
        $category = Category::all();
        return view('admin.category.form',compact('model','category'));
    }

    public function save(Request $rq)
    {
        //         CUSTOM MESSAGES
        $messages = [
            'category_name.required' => 'Vui lòng nhập tên danh mục',
            'category_name.max' => 'Tên danh mục không quá 30 kí tự',
            'slug.required' => 'Đường dẫn không được để trống',
            'slug.unique' => 'Đường dẫn đã tồn tại , vui lòng nhập đường dẫn mới',
            'image.mimes' => 'Không đúng định dạng ảnh , vui lòng chọn đúng định dạng'
        ];

        // Validate
        $rq->validate([
            'category_name' => 'required|max:30',
            'slug' => 'required|unique:categories',
            'image' => 'mimes:jpeg,bmp,png'
        ],$messages);

        if($rq->id != null){
            $model = Category::find($rq->id);
        }else{
            $model = new Category();
        }
        $model->fill($rq->all());


        // upload Image
        if($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid()."-".$file->getClientOriginalName();
            $file->storeAs('uploads',$fileName); //->resize(320, 240)
            $model->image = 'uploads/'.$fileName;
//            dd('done');
        }
        $model->save();
        return redirect(route('category.list'));
    }

    public function remove($id)
    {
        $category = Category::find($id);
        if ($category != null)
        {
            $category->delete();
        }
        return redirect(route('category.list'));
    }

    public function detail($slug)
    {
        $cate = Category::where('slug',$slug)->first();
        $products = Product::where('cate_id',$cate->id)->get();
//            dd($products);
        return view('admin.category.detail',compact('cate','products'));
    }
}
