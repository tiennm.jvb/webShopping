@extends('layouts.AdminTheme')
@section('title','User Form')
@section('content')
    <div class="col-md-6 col-md-offset-3">
        <h2>
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">  User Form</span>
                </div>
            </div>
        </h2>
        <form id="cate-form" action="{{route('user.save')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$model->id}}">
            <div class="form-group">
                <label>Tên: </label>
                <input id="game-name" class="form-control" type="text" name="name" value="{{ old('name',$model->name) }}" placeholder="Nhập tên">
                @if($errors)
                    <span class="text-danger">{{$errors->first('name')}}</span>
                @endif
            </div>

            <div class="form-group">
                <label>Email: </label>
                <input id="email" class="form-control" type="text" name="email" value="{{ old('email',$model->email) }}" placeholder="Email">
                @if($errors)
                    <span class="text-danger">{{$errors->first('email')}}</span>
                @endif
            </div>
            <div class="form-group">
                <label> PassWord: </label>
                <input id="password" class="form-control" type="password" name="password" value="{{ old('password',$model->password) }}" placeholder="Password">
            </div>
            <div class="form-group text-center">
                <label>Ảnh đại diện: </label><br>
                <img class="" src="{{asset($model->image)}}"
                     onerror="this.src='{{asset('default-user.png')}}'"
                     width="200" ><br>
                <input class="form-control" type="file" name="image">
            </div>
            <div class="form-group">
                <label>Role: </label>
                <select class="form-control" name="role" id="">
                    <option value="" selected>--------------------------------------------------------------------------------------------</option>
                    <option @if($model->role == 900)
                                selected
                            @endif
                            value="900">Master admin</option>
                    <option @if($model->role ==500)
                                selected
                            @endif
                            value="600">Admin</option>
                    <option @if($model->role == 100)
                                selected
                            @endif
                            value="300">Member</option>
                </select>
            </div>
            <div class="form-group">
                <label>Trạng thái <i>( Hiển thị trên trang chủ )</i> </label>
                <select class="form-control" id="form_control_1" name="status">
                    <option @if($model->status ==1)
                                selected
                            @endif
                            value="1">Đã kích hoạt</option>
                    <option @if($model->status != 1)
                                selected
                            @endif
                            value="2">Chưa kích hoạt</option>
                </select>
            </div>
            <div class="text-center">
                <button class="btn btn-primary">Lưu</button>
                <a href="{{ route('user.list') }}" class="btn btn-danger">Huỷ</a>
            </div>
        </form>
    </div>
@endsection