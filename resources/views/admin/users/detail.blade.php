@extends('layouts.AdminTheme')
@section('title','User - Detail')
@section('content')
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <a class="img-responsive" hrprofile-sidebaref="{{ route('user.detail', ['id' => $user->id]) }}">
                            <img style="margin-left: 50px;" src="{{asset($user->image)}}"
                                 onerror="this.src='{{asset('default-user.png')}}'"
                                 width="200" >
                        </a>
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name" style="margin-top: 30px;">
                            <h2 class="text-center"><b>{{ $user->name }}</b></h2>
                        </div>
                        <div class="profile-usertitle-job text-center">
                            @if($user->role == 900)
                                <i> Master admin</i>
                            @elseif($user->role == 600)
                                <i> Admin</i>
                            @elseif($user->role == 100)
                                <i> Member</i>
                                @else
                                <i>Other</i>
                            @endif
                        </div>
                    </div>
                    <br>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons text-center">
                        <button type="button" class="btn btn-circle green-haze btn-sm text-center">Follow</button>
                        <button type="button" class="btn btn-circle btn-danger btn-sm text-center">Message</button>
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li>
                                <a href="extra_profile_account.html">
                                    <i class="icon-settings"></i>
                                    Account Settings </a>
                            </li>
                            <li>
                                <a href="page_todo.html" target="_blank">
                                    <i class="icon-check"></i>Trạng thái :
                                    {!! $user->status ==1 ? "<b>Kích hoạt</b>" : "<b>Khóa</b>"!!} </a>
                            </li>
                            <li>
                                <a href="extra_profile_help.html">
                                    <i class="fa fa-envelope-o"></i>
                                    Email : <strong>{{ $user->email }}</strong></a>
                            </li>
                            <li>
                                <a href="extra_profile_help.html">
                                    <i class="icon-info"></i>
                                    Created : <strong>{{ $user->created_at }}</strong></a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
@endsection