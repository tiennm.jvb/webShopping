@extends('layouts.AdminTheme')
@section('title','Category - Detail')
@section('content')
    <div class="col-md-12 offset-0">
        <div class="pricing hover-effect">
            <div class="pricing-head">
                <h3>

                    {{ $products->product_name }}
                    <span>
                            @foreach($cate as $c)
                            @if($c->id == $products->cate_id)
                                <strong style="color: #2c2c2c"> {{ $c->category_name }}</strong>
                            @endif
                        @endforeach
                        </span>
                </h3>
                <h4>
                    <img src="{{asset($products->image)}}"
                         onerror="this.src='{{asset('img-1.jpg')}}'"
                         width="450" ><br>
                </h4>
            </div>
            <ul class="pricing-content list-unstyled">
                <li>
                    <i class="fa fa-tags"></i>
                    Category :
                    @foreach($cate as $c)
                        @if($c->id == $products->cate_id)
                            <a href="{{ route('category.list') }}"> {{ $c->category_name }}</a>
                        @endif
                    @endforeach
                </li>
                <li>
                    <i class="fa fa-asterisk text-center"></i>
                    Price :
                    {{ $products->price }} <span>đ</span>
                </li>
                <li>
                    <i class="fa fa-heart text-center"> </i>
                    Discount price :
                    <span style="color: red">{{ $products->discount_price }} đ</span>
                </li>
                <li>
                    <i class="fa fa-star text-center"></i>
                    Quantity :
                    {{ $products->quantity }}
                </li>
                <li>
                    <i class="fa fa-star text-center"></i>
                    Status :
                    {{ $products->status==1? "active" : "Hide" }}
                </li>
                <li>
                    <i class="fa fa-shopping-cart text-center"></i>
                    Created by :
                </li>
            </ul>
            <div class="pricing-footer">
                <p>
                    {{ $products->description }}
                </p>
                {{--<a href="{{ route('product.edit') }}" class="btn yellow-crusta">--}}
                    {{--Edit <i class="m-icon-swapright m-icon-white"></i>--}}
                {{--</a>--}}
            </div>
        </div>
    </div>
@endsection