<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $rq)
    {
        $keyword = $rq->keyword;
        if ($keyword != null)
        {
            $user =  DB::table('users')->where('name','like',"%$keyword%")
                                        ->orWhere('email','like',"%$keyword%")
                                        ->get();
//            $user = withPath("?keyword=$keyword");
        }else{
            $user = User::all();
        }
        return view('admin.users.list',compact('user','keyword'));
    }
    public function detail($id)
    {
        $user = User::where('id',$id)->first();
        return view('admin.users.detail',compact('user'));
    }

    public function delete($id)
    {
        $user = User::find($id);
        if ($user != null)
        {
            $user->delete();
        }
        return redirect(route('user.list'));
    }

    public function add()
    {
        $model = new User();
        return view('admin.users.form',compact('model'));
    }

    public function edit($id)
    {
        $model = User::where('id',$id)->first();
        $user = new User();
        return view('admin.users.form',compact('model','user'));
    }

    public function save(Request $rq)
    {
//        dd($rq->all());
        $messages = [
            'name.required' => 'Mời bạn nhập tên !',
            'name.max' => 'Tên tối đa chỉ được 20 kí tự !',
            'image.required' => 'Không đúng định dạng ảnh',
            'image.image' => 'Không đúng định dạng anh',
            'email.required' => 'Mời bạn nhập email',
            'email.email' => 'Mời bạn nhập đúng định dạng email',
        ];
        $rq->validate([
            'name' => 'required|max:20',
            'email' => 'required|email',
        ],$messages);
        if ($rq->id !=null)
        {
            $model = User::find($rq->id);
        }else{
            $model = new User();
        }
        $model->fill($rq->all());
        $model->password = Hash::make($rq->password);
//        dd($model);
        if($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid()."-".$file->getClientOriginalName();
            $file->storeAs('uploads',$fileName);
            $model->image = 'uploads/'.$fileName;
//            dd('done');
        }
        $model->save();
        return redirect(route('user.list'));
    }

    public function CheckName(Request $rq)
    {
        $user = User::where('name',$rq->name)->first();
        $result =  $user == false ? true :false;
        return response()->json($result);
    }
}
