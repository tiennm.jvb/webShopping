@extends('layouts.AdminTheme')
@section('title','User Change Password')
@section('content')
    <div class="col-md-6 col-md-offset-3">
        <h2>
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">User Change Password</span>
                </div>
            </div>
        </h2>
        <form id="cate-form" action="#" method="post">
            @csrf
            <div class="form-group">
                <label>Old Password :</label>
                <input id="old_password" class="form-control" type="password" name="old_password">
                @if($errors)
                    <span class="text-danger">{{$errors->first('old_password')}}</span>
                @endif
                @if(session('errMsg'))
                    <span class="text-danger">{{ session('errMsg') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label>New Password :</label>
                <input id="new_password" class="form-control" type="password" name="new_password">
                @if($errors)
                    <span class="text-danger">{{$errors->first('new_password')}}</span>
                @endif
            </div>
            <div class="form-group">
                <label>Confirm Password :</label>
                <input id="cf_password" class="form-control" type="password" name="cf_password">
                @if($errors)
                    <span class="text-danger">{{$errors->first('cf_password')}}</span>
                @endif
            </div>
            <div class="text-center">
                <button class="btn btn-primary">Lưu</button>
                <a href="{{ route('category.list') }}" class="btn btn-danger">Huỷ</a>
            </div>
        </form>
    </div>
@endsection