@extends('layouts.AdminTheme')
@section('title','Slider - Form')
@section('content')
    <div class="col-md-6 col-md-offset-3">
        <h2>
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">  Slider Form</span>
                </div>
            </div>
        </h2>
        <form id="cate-form" action="{{route('slider.save')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$model->id}}">
            <div class="form-group">
                <label>Tên: </label>
                <input id="title" class="form-control" type="text" name="title" value="{{ old('title',$model->title) }}" placeholder="Điền Tiêu đề slider">
                @if($errors)
                    <span class="text-danger">{{$errors->first('title')}}</span>
                @endif
            </div>

            <div class="form-group">
                <div class="div-cate-relative">
                    <label>Đường dẫn: </label>
                    <input id="slug" class="form-control" type="text" name="slug" value="{{ old('slug',$model->slug) }}" placeholder="Đường dẫn website">
                </div>
                @if($errors)
                    <span class="text-danger">{{$errors->first('slug')}}</span>
                @endif
            </div>
            <div class="form-group">
                <label>Ảnh slide: </label>
                <input class="form-control" type="file" name="image">
                @if($errors)
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label>Trạng thái <i>( Hiển thị trên trang chủ )</i> </label>
                <select class="form-control" id="form_control_1" name="status">
                    <option @if($model->status ==1)
                            selected
                            @endif
                            value="1">Đã kích hoạt</option>
                    <option @if($model->status !=1)
                            selected
                            @endif
                            value="2">Chưa kích hoạt</option>
                </select>
            </div>
            <div class="text-center">
                <button class="btn btn-primary">Lưu</button>
                <a href="{{ route('slider.list') }}" class="btn btn-danger">Huỷ</a>
            </div>
        </form>
        <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-asl-form').on('click',function () {
                var product_name = $('#product_name').val();
                product_name = product_name.trim();
                if (product_name ==""|| product_name == null)
                {
                    return false;
                }
                // Gửi request để lấy url mới
                $.ajax({
                    url:'{{ route('getSlug') }}',
                    method:"post",
                    data:{
                        value:product_name,
                        _token: $('#ajaxToken').val()
                    },
                    datatype:"JSON",
                    success: function (rs) {
                        $('#slug').val(rs.data);
                    }

                })
            });

            $('#cate-form').validate({
                rules: {
                    product_name: {
                        required: true,
                        maxlength: 20,
                        checkExisted: '{{ route('products.checkName') }}'
                    },
                    slug: {
                        required: true,
                        checkExisted: '{{ route('products.checkSlug') }}'
                    },
                    image: {
                        extension: "jpg|png|gif"
                    },
                    price: {
                        required: true,
                        min: 0
                    }
                },
                messages: {
                    product_name: {
                        required: 'Vui lòng nhập tên sản phẩm',
                        maxlength: 'Độ dài không vượt quá 10 ký tự'
                    },
                    slug:{
                        required: 'Vui lòng nhập đường dẫn',
                        checkExisted:'Đường dẫn đã tồn tại'
                    },
                    image: {
                        extension: "Không đúng định dạng ảnh"
                    },
                    price: {
                        required: 'Vui lòng nhập giá bán sản phẩm',
                        min: 'Giá không được nhỏ hơn 20000'
                    }
                }
            });
        });
    </script>
@endsection