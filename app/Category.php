<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    public $fillable = ['category_name', 'slug', 'status', 'parent_id'];
    public $timestamps = false;
}
