@extends('layouts.ClientTheme')
@section('title','Product')
@section('content')
<div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
                <div class="leftbar p-r-20 p-r-0-sm">
                    <!--  -->
                    <h4 class="m-text14 p-b-7"style="margin-top: 60px;">
                        Categories
                    </h4>
                    <ul class="p-b-54">
                        <li>
                            <a href="{{ route('home.product') }}">All</a>
                        </li>
                        @foreach($cate as $c)
                            <li class="p-t-4">
                                <a href="{{ route('home.categoryDetail',['slug'=>$c->slug]) }}" class="s-text13 active1">
                                    {{ $c->category_name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="search-product pos-relative bo4 of-hidden">
                        <form>
                            <input class="s-text7 size6 p-l-23 p-r-50" type="text" name="keyword" value="{{ $keyword }}" placeholder="Search Products...">

                            <button type="submit" class="flex-c-m size5 ab-r-m color2 color0-hov trans-0-4">
                                <i class="fs-12 fa fa-search" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
                <!-- Product -->
                <div class="row">
                    @foreach($products as $pr)
                        @if($pr->status == 1)
                        <div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
                            <!-- Block2 -->
                                <div class="block2">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                    <a href="{{ route('home.productDetail',['slug'=>$pr->slug]) }}">
                                        <img src="{{ asset($pr->image) }}" width="270" height="360" alt="IMG-PRODUCT">
                                    </a>

                                    <div class="block2-overlay trans-0-4">
                                        <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                            <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                            <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                        </a>

                                        <div class="block2-btn-addcart w-size1 trans-0-4">
                                            <!-- Button -->
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
                                                Add to Cart
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="block2-txt p-t-20">
                                    <a href="{{ route('home.productDetail',['slug'=>$pr->slug]) }}" class="block2-name dis-block s-text3 p-b-5">
                                        {{ $pr->product_name  }}
                                    </a>

                                    <span class="block2-price m-text6 p-r-5">
                                            $ {{ $pr->price }}
                                        </span>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection