<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class NewController extends Controller
{
    public function index(Request $rq)
    {
        $keyword = $rq->keyword;
        if ($keyword != null)
        {
            $products = News::where('title','like',"%$keyword%")->get();
        }else{
            $products = News::paginate(5);
        }
        $news = News::all();
//        dd($cate);
        return view('admin.news.list',compact('products','news','keyword'));
    }

    public function add()
    {
        $model = new News();
        return view('admin.news.form',compact('model'));
    }

    public function edit($id)
    {
        $model = News::find($id);
//        dd($model);
        return view('admin.news.form',compact('model'));
    }

    public function save(Request $rq)
    {

        //      Custom Messages

        $messages = [
            'title.required' => 'Mời bạn nhập tiêu đề',
            'title.max' => 'Tiêu đề không được quá 70 kí tụ',
            'slug.required' => 'Mời bạn nhập đường dẫn',
            'slug.unique' => 'Đường dẫn đã tồn tại , vui lòng nhập đường dẫn mới',
            'image.image'=>'Mời bạn chọn đúng định dạng ảnh'
        ];
        $rq->validate([
            'title' => 'required|max:90',
            'slug' => 'required|unique:news',
            'image'=>'image'
        ],$messages);

        if ($rq->id != null)
        {
            $model= News::find($rq->id);
        }else{
            $model= new News();
        }
        $model->fill($rq->all());

        // upload file
        if($rq->hasFile('image'))
        {
            $file = $rq->file('image');
            $fileName = uniqid()."-".$file->getClientOriginalName();
            $file->storeAs('uploads',$fileName);
            $model->image = 'uploads/'.$fileName;
//            dd('done');
        }
        $model->save();
        return redirect(route('news.list'));
    }

    public function remove($id)
    {
        $product = News::find($id);
        if ($product != null)
        {
            $product->delete();
        }
        return redirect(route('news.list'));
    }

    public function detail($slug = null)
    {
        $products = News::where('slug',$slug)->first();
//        $cate = Category::all();
//        dd($products);
        return view('admin.news.detail',compact('products','cate'));
    }
}
