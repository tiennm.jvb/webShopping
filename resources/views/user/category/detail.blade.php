@extends('layouts.ClientTheme')
@section('title','Product Detail')
@section('content')
    <br><br><br>
<div class="wrap-slick2">
    @if($product->status == 1)
        <div class="slick2">
                <div class="item-slick2 p-l-15 p-r-15">
                    <!-- Block2 -->
                    <div class="block2">

                        <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                            <img src="{{asset($product->image)}}" onerror="this.src='{{ asset('img-1.jpg') }}'" width="200px;" alt="IMG-PRODUCT">

                            <div class="block2-overlay trans-0-4" id="pro-1">
                                <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                </a>

                                <div class="block2-btn-addcart w-size1 trans-0-4">
                                    <!-- Button -->
                                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4 add-cart" rel=""    >
                                        Add to Cart
                                    </button>
                                    <input type="hidden"  value="1" class="countNumber">
                                </div>
                            </div>
                        </div>

                        <div class="block2-txt p-t-20">
                            <a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5 " id="ProductName">
                                {{ $product->product_name }}
                            </a>

                            <span class="block2-price m-text6 p-r-5">
									$ {{ $product->price }}
								</span>
                        </div>
                    </div>
                </div>
            <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
        </div>
    @endif
    </div>
@endsection
