<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    public $fillable = ['product_id','new_id','content','user_id'];
    public  $timestamps=false;
}
