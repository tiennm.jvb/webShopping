@extends('layouts.AdminTheme')
@section('title','Review')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            {{--<a href="{{ route('product.add') }}">--}}
                {{--<button id="sample_editable_1_new" class="btn green">--}}
                    {{--Add New  <i class="fa fa-plus"></i>--}}
                {{--</button>--}}
            {{--</a>--}}
            <form action="">
{{--                <input value="{{ $keyword }}" name="keyword" type="text" class="table-group-action-input form-control input-inline" placeholder="" aria-controls="sample_1">--}}
                <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
            </form>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Product Name
                    </th>
                    <th class="hidden-480">
                        Image
                    </th>
                    <th class="hidden-480">
                        Price
                    </th>
                    <th class="hidden-480">
                        Category
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Author
                    </th>
                    <th>

                    </th>
                </tr>
                </thead>
                <tbody>
                {{--@if($products != null)--}}
                    {{--@foreach($products as $pr)--}}
                        {{--<tr>--}}
                            {{--<td>--}}
                                {{--{{ $pr->id }}--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<a href="{{ route('product.detail', ['slug' => $pr->slug]) }}">{{ $pr->product_name }}</a>--}}
                            {{--</td>--}}
                            {{--<td class="hidden-480">--}}
                                {{--<a href="{{ route('product.detail', ['slug' => $pr->slug]) }}">--}}
                                    {{--<img src="{{asset($pr->image)}}"--}}
                                         {{--onerror="this.src='{{asset('default-img.jpg')}}'"--}}
                                         {{--width="100" >--}}
                                {{--</a>--}}
                            {{--</td>--}}
                            {{--<td class="hidden-480">--}}
                                {{--{{ $pr->price }}--}}
                            {{--</td>--}}
                            {{--<td class="hidden-480">--}}
                                {{--@foreach($cate as $c)--}}
                                    {{--@if($c->id == $pr->cate_id)--}}
                                        {{--<p>{{ $c->category_name }}</p>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<p>{{$pr->status==1 ? "Da kich hoat" :"Chua kich hoat"}}</p>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--@foreach($user as $u)--}}
                                    {{--@if($u->id == $pr->user_id)--}}
                                        {{--<p>{{ $u->name }}</p>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<a href="javascript:;"--}}
                                   {{--onclick="confirmRomove('{{ route('product.remove',['id'=>$pr->id]) }}')"--}}
                                {{-->Remove</a>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                                {{--<a href="{{ route('product.edit',['id'=>$pr->id]) }}">Edit</a>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    {{--@endforeach--}}
                {{--@endif--}}
                </tbody>
            </table>
        </div>
    </div>
@section('js')
    <script type="text/javascript">
        function confirmRomove(url)
        {
            bootbox.confirm({
                message: "This is a confirm with custom button text and color! Do you like it?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        window.location.href = url;
                    }
                    console.log(result);
                }
            });
        }
    </script>
@endsection
@endsection