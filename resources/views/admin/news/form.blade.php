@extends('layouts.AdminTheme')
@section('title','News - Form')
@section('content')
<div class="col-md-6 col-md-offset-3">
    <h2>
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-settings font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">  News Form</span>
            </div>
        </div>
    </h2>
    <form id="cate-form" action="{{route('news.save')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$model->id}}">
        <div class="form-group">
            <label>Tên: </label>
            <input id="title" class="form-control" type="text" name="title"
                   value="{{ old('title',$model->title) }}" placeholder="Điền tiêu đề bài viết">
            @if($errors)
                <span class="text-danger">{{$errors->first('title')}}</span>
            @endif
        </div>
        <div class="form-group">
            <div class="div-cate-relative">
                <label>Đường dẫn: </label>
                <input id="slug" class="form-control" type="text" name="slug" value="{{ old('slug',$model->slug) }}"
                       placeholder="Đường dẫn website">
                <button class="btn btn-success btn-asl-form" type="submit"><i class="fa fa-bolt"></i></button>
            </div>
            @if($errors)
                <span class="text-danger">{{$errors->first('slug')}}</span>
            @endif
        </div>
        <div class="form-group">
            <label>Ảnh bài viết: </label>
            <input class="form-control" type="file" name="image">
            @if($errors)
                <span class="text-danger">{{ $errors->first('image') }}</span>
            @endif
        </div>
        <div class="form-group">
            <label>Trạng thái <i>( Hiển thị trên trang chủ )</i> </label>
            <select class="form-control" id="form_control_1" name="status">
                <option @if($model->status ==1)
                        selected
                        @endif
                        value="1">Đã kích hoạt</option>
                <option @if($model->status !=1)
                        selected
                        @endif
                        value="2">Chưa kích hoạt</option>
            </select>
        </div>
        <div class="form-group">
            <label>Mô tả: </label>
            <textarea id="editor" name="description" class="form-control">{!! $model->description !!}</textarea>
        </div>
        <div class="text-center">
            <button class="btn btn-primary">Lưu</button>
            <a href="{{ route('category.list') }}" class="btn btn-danger">Huỷ</a>
        </div>
    </form>
    <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
</div>
@endsection
@section('js')
    <script !src="">
        $(document).ready(function () {
            $('#cate-form').validate({
                rules : {
                    category_name : 'required',
                    slug : 'required'
                }
            });
        });
    </script>
@endsection