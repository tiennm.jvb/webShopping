@extends('layouts.AdminTheme')
@section('title','Slider')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{ route('slider.add') }}">
                <button id="sample_editable_1_new" class="btn green">
                    Add New  <i class="fa fa-plus"></i>
                </button>
            </a>
            <form action="">
                <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
            </form>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Title
                    </th>
                    <th>
                        Image
                    </th>
                    <th class="hidden-480">
                        Status
                    </th>
                    <th class="hidden-480">
                        Slug
                    </th>
                    <th>

                    </th>
                </tr>
                </thead>
                <tbody>
                @if($slider != null)
                    @foreach($slider as $sl)
                        <tr>
                            <td>
                                {{ $sl->id }}
                            </td>
                            <td>
                                {{ $sl->title }}
                            </td>
                            <td>
                                <img src="{{ asset($sl->image) }}" alt="{{ $sl->title }}" width="200">
                            </td>
                            <td>
                                <p>{{ $sl->status ==1 ? "Đã kích hoạt" : "Chưa kích hoạt" }}</p>
                            </td>
                            <td>{{ $sl->slug }}</td>
                            <td><a href="{{ route('slider.edit',['id'=>$sl->id]) }}"><i class="fa fa-edit"></i>edit</a></td>
                            <td>
                                <a href="javascript:;" onclick="confirmRomove('{{ route('slider.delete',['id'=>$sl->id]) }}')">
                                    <i class="fa fa-edit"></i>Remove
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@section('js')
    <script type="text/javascript">
        function confirmRomove(url)
        {
            bootbox.confirm({
                message: "This is a confirm with custom button text and color! Do you like it?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        window.location.href = url;
                    }
                    console.log(result);
                }
            });
        }
        $(document).ready(function () {
            $('#pageSize').on('change',function () {
                console.log($(this).val);
            });
        });
    </script>
@endsection
@endsection