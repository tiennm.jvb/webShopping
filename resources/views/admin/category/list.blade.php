@extends('layouts.AdminTheme')
@section('title','Category')
@section('content')
<div class="row">
    <div class="col-xs-12">
        <a href="{{ route('category.add') }}">
            <button id="sample_editable_1_new" class="btn green">
                Add New  <i class="fa fa-plus"></i>
            </button>
        </a>
        <form action="">
            <input name="keyword" type="text" class="table-group-action-input form-control input-inline" placeholder="" aria-controls="sample_1">
            <button type="submit" class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
        </form>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>
                    #
                </th>
                <th>
                    Category Name
                </th>
                <th>
                    Image
                </th>
                <th class="hidden-480">
                    Status
                </th>
                <th class="hidden-480">

                </th>
                <th>
                </th>
            </tr>
            </thead>
            <tbody>
            @if($category != null)
                @foreach($category as $cate)
                <tr>
                    <td>
                        {{ $cate->id }}
                    </td>
                    <td>
                        <a href="{{ route('category.detail',['slug'=>$cate->slug]) }}">{{ $cate->category_name }}</a>
                    </td>
                    <td>
                        <img src="{{ asset($cate->image) }}" alt="" width="200">
                    </td>
                    <td>
                        <p>{{ $cate->status ==1 ? "Đã kích hoạt" : "Chưa kích hoạt" }}</p>
                    </td>
                    <td><a href="{{ route('category.edit',['id'=>$cate->id]) }}"><i class="fa fa-edit"></i>edit</a></td>
                    <td>
                        <a href="javascript:;" onclick="confirmRomove('{{ route('category.remove',['id'=>$cate->id]) }}')">
                            <i class="fa fa-edit"></i>Remove
                        </a>
                    </td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
@section('js')
    <script type="text/javascript">
        function confirmRomove(url)
        {
            bootbox.confirm({
                message: "This is a confirm with custom button text and color! Do you like it?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        window.location.href = url;
                    }
                    console.log(result);
                }
            });
        }
        $(document).ready(function () {
            $('#pageSize').on('change',function () {
                console.log($(this).val);
            });
        });
    </script>
@endsection
@endsection