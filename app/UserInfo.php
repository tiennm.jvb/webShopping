<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table = 'user_info';
    public $fillable = ['fullname','facebook','user_id','address','phone','image','created_at','updated_at'];
}
