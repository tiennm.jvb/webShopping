<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class UserLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/user.login';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login()
    {
        return view('user.login');
    }

    public function postLogin(Request $rq)
    {
//        dd($rq->all());
        if (Auth::attempt(['email' => $rq->email, 'password' => $rq->password])) {
            return redirect(route('home'));
        }else{
            return back()->with('msg','Sai tài khoản hoặc mật khẩu');
        }
    }
}
