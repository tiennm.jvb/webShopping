@extends('layouts.AdminTheme')
@section('title','Dashboard')
@section('content')
    <div class="row text-center">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ $product }}
                    </div>
                    <div class="desc">
                        Product
                    </div>
                </div>
                <a class="more" href="{{ route('product.list') }}">
                    View more <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ $user }}
                    </div>
                    <div class="desc">
                        User
                    </div>
                </div>
                <a class="more" href="{{ route('user.list') }}">
                    View more <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="dashboard-stat green-haze col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    {{ $news }}
                </div>
                <div class="desc">
                    New
                </div>
            </div>
            <a class="more" href="{{ route('news.list') }}">
                View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red-intense">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{ $cate }}
                    </div>
                    <div class="desc">
                        Category
                    </div>
                </div>
                <a class="more" href="{{ route('category.list') }}">
                    View more <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>
@endsection