@extends('layouts.AdminTheme')
@section('title','Category - Detail')
@section('content')
@foreach($products as $p)
<div class="col-md-4">
    <div class="pricing hover-effect">
        <div class="pricing-head">
            <h3><a href="{{ route('product.detail', ['slug' => $p->slug]) }}">{{ $p->product_name }}</a> <span>{{ $p->product_code }}</span>
            </h3>
            <h4>{{ $p->price }}<i>$</i>
                <span>{{ $p->discount_price }} </span>
                <img src="{{asset($p->image)}}"
                     onerror="this.src='{{asset('img-1.jpg')}}'"
                     width="200" height="250"><br>
            </h4>
        </div>
        <ul class="pricing-content list-unstyled">
            <li>
                <i class="fa fa-tags"></i> {{ $cate->category_name }}
            </li>
            <li>
                <i class="fa fa-asterisk"></i> @if($p->status ==1) Đã kích hoạt @else Chưa kích hoạt @endif
            </li>
            <li>
                <i class="fa fa-ticket"></i> {{ $p->quantity }}
            </li>
        </ul>
        <div class="pricing-footer">
            <p>
                {{ $p->description }}
            </p>
            <a href="{{ route('product.detail', ['slug' => $p->slug]) }}" class="btn yellow-crusta">
               Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
@endforeach
@endsection