<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <a href="#" class="{{ (\Request::route()->getName() == 'this.route') ? 'active' : '' }}">
            <li class="">
                <a href="{{ route('category.list') }}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li>
                @if(Auth::user()->role >=900)
                    <a href="javascript:;">
                        <i class="icon-user-following"></i>
                        <span class="title">User</span>
                        <span class="arrow "></span>
                    </a>
                @endif
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('user.list') }}"><i class="fa fa-file-text"></i> List User</a>
                    </li>
                    <li>
                        <a href="{{ route('user.add') }}"> <i class="fa fa-plus-square"></i> Add User</a>
                    </li>
                </ul>
            </li>
            <li>
                @if(Auth::user()->role >= 900)
                    <a href="javascript:;">
                        <i class="icon-wallet"></i>
                        <span class="title">Category</span>
                        <span class="arrow "></span>
                    </a>
                @endif
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('category.list') }}"><i class="fa fa-file-text"></i> List Category</a>
                    </li>
                    <li>
                        <a href="{{ route('category.add') }}"> <i class="fa fa-plus-square"></i> Add Category</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-handbag"></i>
                    <span class="title">Product</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('product.list') }}"><i class="fa fa-file-text"></i> List Product</a>
                    </li>
                    <li>
                        <a href="{{ route('product.add') }}"> <i class="fa fa-plus-square"></i> Add Product</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-speech"></i>
                    <span class="title">News</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('news.list') }}"><i class="fa fa-file-text"></i> List news</a>
                    </li>
                    <li>
                        <a href="{{ route(('news.add')) }}"> <i class="fa fa-plus-square"></i>  Add new </a>
                    </li>
                </ul>
            </li>
            <!-- BEGIN ANGULARJS LINK -->
            <!-- END ANGULARJS LINK -->
            <li>
                @if(Auth::user()->role >=900)
                    <a href="javascript:;">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Rating</span>
                        <span class="arrow "></span>
                    </a>
                @endif
                <ul class="sub-menu">
                    <li>
                        <a href="form_controls_md.html"><i class="fa fa-file-text"></i> List Rating</a>
                    </li>
                </ul>
            </li>
            <li>
                @if(Auth::user()->role >=900)
                    <a href="javascript:;">
                        <i class="fa fa-comments"></i>
                        <span class="title">Comment</span>
                        <span class="arrow "></span>
                    </a>
                @endif
                <ul class="sub-menu">
                    <li>
                        <a href="ecommerce_index.html"><i class="fa fa-file-text"></i> List Comment</a>
                    </li>
                </ul>
            </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>