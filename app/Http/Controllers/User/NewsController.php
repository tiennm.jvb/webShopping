<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Product;
use App\Category;
use App\User;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::all();
        $product = Product::all()->take(4);
        $cate = Category::all();
        $user = User::all();
        return view('user.news.list',compact('news','product','cate','user'));
    }

    public function detail($slug)
    {
        $news = News::where('slug',$slug)->first();
        $cate = Category::all();
        $product = Product::all();
//        dd($news);
        return view('user.news.detail',compact('news','cate','product'));
    }
}
