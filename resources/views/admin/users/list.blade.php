@extends('layouts.AdminTheme')
@section('title','User')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{ route('user.add') }}">
                <button id="sample_editable_1_new" class="btn green">
                    Add New  <i class="fa fa-plus"></i>
                </button>
            </a>
            <form>
                <input type="text" name="keyword" value="{{ $keyword }}">
                <button type="submit">Search</button>
            </form>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Name
                    </th>
                    <th class="hidden-480">
                        Image
                    </th>
                    <th class="hidden-480">
                        Status
                    </th>
                    <th class="hidden-480">
                        Email
                    </th>
                    <th>
                        Role
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user as $u)
                        <tr>
                            <td>
                                {{ $u->id }}
                            </td>
                            <td>
                                <a href="{{ route('user.detail', ['id' => $u->id]) }}">{{ $u->name }}</a>
                            </td>
                            <td class="hidden-480">
                                <a href="{{ route('user.detail', ['id' => $u->id]) }}">
                                    <img src="{{asset($u->image)}}"
                                         onerror="this.src='{{asset('default-user.png')}}'"
                                         width="50" >
                                </a>
                            </td>
                            <td class="hidden-480">
                                @if( $u->status === 1)
                                        Kích hoạt
                                    @else
                                        Khóa
                                @endif
                            </td>
                            <td class="hidden-480">
                                {{ $u->email }}
                            </td>
                            <td>
                                @if($u->role == 900)
                                        Master Admin
                                    @elseif($u->role == 500)
                                        Admin
                                    @elseif($u->role == 100)
                                        Member
                                    @else
                                        Other
                                @endif
                            </td>
                            <td>
                                <a href="javascript:;" onclick="confirmRomove('{{ route('user.delete',['id'=>$u->id]) }}')">
                                    Delete
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('user.edit',['id'=>$u->id]) }}">
                                    Update
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        function confirmRomove(url)
        {
            bootbox.confirm({
                message: "This is a confirm with custom button text and color! Do you like it?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result)
                    {
                        window.location.href = url;
                    }
                    console.log(result);
                }
            });
        }
        $(document).ready(function () {
            $('#pageSize').on('change',function () {
                console.log($(this).val);
            });
        });
    </script>
@endsection