<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    public $fillable=['product_name','product_code','description','price','quantity','status','slug','cate_id','image','user_id','discount_price','created_at'];
    public $timestamps=false;
}
