<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table='reviews';
    public $fillable=['product_id','user_id','rating','comments'];
    public $timestamps=false;
}
